﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDrawer
{
    public enum EdgeDirection
    {
        GoTo,
        BackTo,
        BackFrom,
        GoFrom,
    }
}
