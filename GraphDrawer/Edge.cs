﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDrawer
{
    public class Edge
    {
        public Vertex srcVertex;
        public Vertex desVertex;

        private bool backEdge;
        public bool BackEdge { get { return backEdge; } }

        public bool Marked { get; set; }

        public Edge(Vertex srcVertex, Vertex desVertex, bool backEdge)
        {
            this.srcVertex = srcVertex;
            this.desVertex = desVertex;
            this.backEdge = backEdge;
            Marked = false;
        }

        public override string ToString()
        {
            return "srcDFN=" + srcVertex.DFN + ";desDFN=" + desVertex.DFN + ";IsBack=" + backEdge + ";Marked=" + Marked;
        }
    }
}
