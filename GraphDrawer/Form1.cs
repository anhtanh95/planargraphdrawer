﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphDrawer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[][] a = new int[6][];
            //a[0] = new int[] { 0, 1, 1, 0, 1, 1 };
            //a[1] = new int[] { 1, 0, 1, 1, 1, 0 };
            //a[2] = new int[] { 1, 1, 0, 1, 0, 1 };
            //a[3] = new int[] { 0, 1, 1, 0, 1, 1 };
            //a[4] = new int[] { 1, 1, 0, 1, 0, 1 };
            //a[5] = new int[] { 1, 0, 1, 1, 1, 0 };
            a[0] = new int[] { 0, 1, 1, 1, 0, 0 };
            a[1] = new int[] { 1, 0, 1, 1, 0, 0 };
            a[2] = new int[] { 1, 1, 0, 1, 1, 1 };
            a[3] = new int[] { 1, 1, 1, 0, 0, 1 };
            a[4] = new int[] { 0, 0, 1, 0, 0, 1 };
            a[5] = new int[] { 0, 0, 1, 1, 1, 0 };
            Numbering.st_Numbering(a, 0, 2);

            Point[] pts = {new Point(100,100), new Point (120,120)};
            panel1.CreateGraphics().DrawLines(new Pen(Brushes.Black, 4), pts);
        }
    }
}
