﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDrawer
{
    public class TreeNode : IDisposable
    {
        public TreeNode(Object Value)
        {
            this.Value = Value;
            Parent = null;
        }

        public TreeNode(TreeNode Parent, Object Value)
        {
            this.Value = Value;
            this.Parent = Parent;
        }

        public TreeNode Root
        {
            get
            {
                //return (Parent == null) ? this : Parent.Root;
                TreeNode node = this;
                while (node.Parent != null)
                {
                    node = node.Parent;
                }
                return node;
            }
        }

        TreeNode _Parent;
        public TreeNode Parent
        {
            get { return _Parent; }
            set
            {
                if (value == _Parent)
                {
                    return;
                }
                if (_Parent != null)
                {
                    _Parent.Children.Remove(this);
                }
                if (value != null && !value.Children.Contains(this))
                {
                    value.Children.Add(this);
                }
                _Parent = value;
            }
        }

        List<TreeNode> _Children = new List<TreeNode>();
        public List<TreeNode> Children
        {
            get { return _Children; }
            private set { _Children = value; }
        }

        private Object _Value;
        public Object Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
            }
        }

        public int Depth
        {
            get
            {
                //return (Parent == null ? -1 : Parent.Depth) + 1;
                int depth = 0;
                TreeNode node = this;
                while (node.Parent != null)
                {
                    node = node.Parent;
                    depth++;
                }
                return depth;
            }
        }

        public enum TreeTraversalType
        {
            TopDown,
            BottomUp
        }

        private TreeTraversalType _DisposeTraversal = TreeTraversalType.BottomUp;
        public TreeTraversalType DisposeTraversal
        {
            get { return _DisposeTraversal; }
            set { _DisposeTraversal = value; }
        }

        private bool _IsDisposed;
        public bool IsDisposed
        {
            get { return _IsDisposed; }
        }

        public void Dispose()
        {
            CheckDisposed();
            OnDisposing();
            // clean up contained objects (in Value property)
            if (Value is IDisposable)
            {
                if (DisposeTraversal == TreeTraversalType.BottomUp)
                {
                    foreach (TreeNode node in Children)
                    {
                        node.Dispose();
                    }
                }
                (Value as IDisposable).Dispose();
                if (DisposeTraversal == TreeTraversalType.TopDown)
                {
                    foreach (TreeNode node in Children)
                    {
                        node.Dispose();
                    }
                }
            }
            _IsDisposed = true;
        }

        public event EventHandler Disposing;
        protected void OnDisposing()
        {
            if (Disposing != null)
            {
                Disposing(this, EventArgs.Empty);
            }
        }

        public void CheckDisposed()
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        public override string ToString()
        {
            string Description = "";
            if (Value != null)
            {
                Description = "[" + Value.ToString() + "] ";
            }
            return Description + "Depth=" + Depth.ToString() + ", Children=" + Children.Count.ToString();
        }
    }
}
