﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDrawer
{
    public class Vertex
    {
        public int vertexID;
        public int DFN;//Depth First Number
        public int LOW;//Low Point
        public int STN;//st-Numbering
        public Vertex FATH;
        public bool Traced { get; set; }
        public LinkedList<int> AdjList;
        public Dictionary<EdgeDirection, List<Edge>> AdjEdge = new Dictionary<EdgeDirection, List<Edge>>();

        public Vertex(int vertexID)
        {
            this.vertexID = vertexID;
            DFN = 0;
            LOW = 0;
            STN = 0;
            FATH = null;
            Traced = false;
            AdjList = new LinkedList<int>();
        }

        public void AddAdjEdge(Edge edge)
        {
            if ((edge.srcVertex.vertexID == vertexID || edge.desVertex.vertexID == vertexID))
            {
                EdgeDirection edgeDirection = Numbering.GetEdgeDirectionByVertex(edge, this);
                if (!AdjEdge.ContainsKey(edgeDirection)) AdjEdge[edgeDirection] = new List<Edge>();
                AdjEdge[edgeDirection].Add(edge);
            }
        }

        public override string ToString()
        {
            return base.ToString() + ";DFN=" + DFN + ";STN=" + STN;
        }
    }
}
