﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDrawer
{
    public class Numbering
    {
        private static int[][] Matrix;
        private static Random random = new Random();
        private static Dictionary<int, Vertex> AdjLists = new Dictionary<int, Vertex>();
        private static List<Edge> EdgeList = new List<Edge>();

        private static TreeNode Tree;

        private static int CountDFN;
        private static int CountSTN;

        public static void st_Numbering(int[][] a, int t, int s)
        {
            Matrix = a;
            for (int i = 0; i < Matrix.Length; i++)
            {
                Vertex vertex = new Vertex(i);
                AdjLists.Add(i, vertex);
            }

            ResetTraced();

            CountDFN = 1;
            Vertex tVertex = AdjLists[t];
            tVertex.Traced = true;
            tVertex.DFN = CountDFN;
            tVertex.LOW = tVertex.DFN;
            Edge root = new Edge(null, tVertex, false);
            root.Marked = true;
            Tree = new TreeNode(root);//tree root
            CountDFN++;

            Vertex sVertex = AdjLists[s];//AdjLists[random.Next(0, Matrix.Length)];
            sVertex.FATH = tVertex;
            Edge tsEdge = new Edge(tVertex, sVertex, false);
            TreeNode newNode = new TreeNode(Tree, tsEdge);
            //tVertex.AddAdjEdge(stEdge);
            EdgeList.Add(tsEdge);

            DFSearch(newNode, sVertex);

            foreach (Vertex vertex in AdjLists.Values)
            {
                foreach (Edge edge in EdgeList)
                {
                    vertex.AddAdjEdge(edge);
                }
            }

            ResetTraced();
            tVertex.Traced = true;
            sVertex.Traced = true;
            tsEdge.Marked = true;
            Compute_stNumber(tVertex, sVertex);
            Console.Write("");
        }

        private static void DFSearch(TreeNode parent, Vertex srcVertex)
        {
            srcVertex.Traced = true;
            srcVertex.DFN = CountDFN;
            srcVertex.LOW = srcVertex.DFN;
            CountDFN++;

            //Loop through all adjacency vertices of source vertex
            for (int i = 0; i < Matrix.Length; i++)
            {
                int nextVertex = (srcVertex.vertexID + i + 1) % Matrix.Length;
                if (Matrix[srcVertex.vertexID][nextVertex] == 1)
                {
                    Vertex desVertex = AdjLists[nextVertex];
                    if (!desVertex.Traced)
                    {
                        //Add Edge to Tree
                        Edge newEdge = new Edge(srcVertex, desVertex, false);
                        TreeNode newNode = new TreeNode(parent, newEdge);
                        if (!IsContainsEdge(newEdge))
                        {
                            //srcVertex.AddAdjEdge(newEdge);
                            EdgeList.Add(newEdge);
                        }

                        //Set Father for destination vertex
                        desVertex.FATH = srcVertex;

                        //Continue Depth-Fisrt Search
                        DFSearch(newNode, desVertex);

                        //Set Low Point for source vertex
                        srcVertex.LOW = Math.Min(srcVertex.LOW, desVertex.LOW);
                    }
                    else if (srcVertex.FATH != desVertex)
                    {
                        //Set Low Point for source vertex
                        srcVertex.LOW = Math.Min(srcVertex.LOW, desVertex.DFN);

                        //Add Back Edge to Tree
                        Edge backToEdge = new Edge(srcVertex, desVertex, true);
                        TreeNode newNode = new TreeNode(parent, backToEdge);
                        if (!IsContainsEdge(backToEdge))
                        {
                            //srcVertex.AddAdjEdge(backToEdge);
                            EdgeList.Add(backToEdge);
                        }
                    }
                }
            }
        }

        private static void ResetTraced()
        {
            foreach (Vertex vertex in AdjLists.Values)
            {
                vertex.Traced = false;
            }
        }

        private static void FindGoPath(Edge edge, Stack<Vertex> path)
        {
            edge.Marked = true;
            path.Push(edge.desVertex);
            foreach (Edge subEdge in EdgeList)
            {
                if (!subEdge.Marked
                && edge.desVertex.vertexID == subEdge.srcVertex.vertexID
                && subEdge.srcVertex.LOW == subEdge.desVertex.DFN
                && GetEdgeDirectionByVertex(subEdge, edge.desVertex) == EdgeDirection.BackTo)
                {
                    subEdge.Marked = true;
                    path.Push(subEdge.desVertex);
                    return;
                }
            }
            //if there is no back edge
            foreach (Edge subEdge in EdgeList)
            {
                if (!subEdge.Marked
                && edge.desVertex.vertexID == subEdge.srcVertex.vertexID
                && GetEdgeDirectionByVertex(subEdge, edge.desVertex) == EdgeDirection.GoTo)
                {
                    FindGoPath(subEdge, path);
                    return;
                }
            }
        }

        private static void FindBackPath(Vertex vertex, Stack<Vertex> path)
        {
            path.Push(vertex);
            Vertex father = vertex.FATH;
            if (father == null) return;
            Edge edge = GetPathByVertex(father, vertex);
            if (edge != null && !edge.Marked)
            {
                edge.Marked = true;//mark edge
                FindBackPath(father, path);
            }
        }

        private static Edge GetPathByVertex(Vertex start, Vertex end)
        {
            foreach (Edge edge in EdgeList)
            {
                if (edge.srcVertex.vertexID == start.vertexID && edge.desVertex.vertexID == end.vertexID)
                {
                    return edge;
                }
            }
            return null;
        }

        private static Stack<Vertex> PATH(Vertex v)
        {
            TreeNode pointer = Tree;
            Stack<Vertex> path = new Stack<Vertex>();
            path.Push(v);

            if (v.AdjEdge.ContainsKey(EdgeDirection.BackTo))
            {
                foreach (Edge edge in v.AdjEdge[EdgeDirection.BackTo])
                {
                    if (!edge.Marked)
                    {
                        edge.Marked = true;
                        path.Push(edge.desVertex);
                        return path;
                    }
                }
            }
            if (v.AdjEdge.ContainsKey(EdgeDirection.GoTo))
            {
                foreach (Edge edge in v.AdjEdge[EdgeDirection.GoTo])
                {
                    if (!edge.Marked)
                    {
                        FindGoPath(edge, path);
                        return path;
                    }
                }
            }
            if (v.AdjEdge.ContainsKey(EdgeDirection.BackFrom))
            {
                for (int i = v.AdjEdge[EdgeDirection.BackFrom].Count - 1; i >= 0; i--)
                {
                    Edge edge = v.AdjEdge[EdgeDirection.BackFrom][i];
                    if (!edge.Marked)
                    {
                        edge.Marked = true;
                        FindBackPath(edge.srcVertex, path);
                        return path;
                    }
                }
            }
            return path;
        }

        private static void Compute_stNumber(Vertex t, Vertex s)
        {
            Stack<Vertex> S = new Stack<Vertex>();
            S.Push(t);//a=t
            S.Push(s);//c=s
            CountSTN = 1;

            Vertex v = S.Pop();
            while (v.vertexID != t.vertexID)
            {
                
                Stack<Vertex> path = PATH(v);
                if (path.Count > 1)
                {
                    while (path.Count > 0)
                    {
                        Vertex vertex = path.Pop();
                        if (!S.Any(a => a.vertexID == vertex.vertexID))
                        {
                            S.Push(vertex);
                        }
                    }
                }
                else
                {
                    v.STN = CountSTN;
                    CountSTN++;
                }
                v = S.Pop();
            }
            v.STN = CountSTN;
        }

        private static bool IsContainsEdge(Edge edge)
        {
            foreach (Edge x in EdgeList)
            {
                if ((x.srcVertex.vertexID == edge.srcVertex.vertexID && x.desVertex.vertexID == edge.desVertex.vertexID)
                        || (x.srcVertex.vertexID == edge.desVertex.vertexID && x.desVertex.vertexID == edge.srcVertex.vertexID))
                    return true;
            }
            return false;
        }

        public static EdgeDirection GetEdgeDirectionByVertex(Edge edge, Vertex vertex)
        {
            if (vertex.vertexID == edge.srcVertex.vertexID)
            {
                return edge.BackEdge ? EdgeDirection.BackTo : EdgeDirection.GoTo;
            }
            else
            {
                return edge.BackEdge ? EdgeDirection.BackFrom : EdgeDirection.GoFrom;
            }
        }

        public static Vertex GetVertexByDFN(int DFN)
        {
            foreach (Vertex vertex in AdjLists.Values)
            {
                if (vertex.DFN == DFN) return vertex;
            }
            return null;
        }
    }
}
